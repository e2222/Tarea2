defmodule Tarea2 do
  @moduledoc """
  Documentation for `Tarea2`.
  """

  @doc """
  This function convert a list of list in a single list.

  ## Examples

      iex> list = [1, [2,3], [4], [5,6,7]]
      iex> Tarea2.flatten(list)
      [1,2,3,4,5,6,7]
  """
  def flatten(list) do
    ##IO.puts("1  List: #{inspect(list)} ") ##For debugging purpose
    flatten(list, []) |> Enum.reverse
  end

  defp flatten([], acc) do
    ##IO.puts("2  acc: #{inspect(acc)}") ##For debugging purpose
    acc
  end

  defp flatten([ []|tail], acc) do
    ##IO.puts("3  Tail: #{inspect(tail)}  acc: #{inspect(acc)}") ##For debugging purpose
    flatten(tail, acc)
  end

  defp flatten([head | tail], acc) when is_list(head) do
    ##IO.puts("4  Head: #{inspect(head)}  Tail: #{inspect(tail)}  acc: #{inspect(acc)}") ##For debugging purpose
      flatten(tail, flatten(head, acc))
  end

  defp flatten([head | tail], acc) do
    ##IO.puts("5  Head: #{inspect(head)}  Tail: #{inspect(tail)}  acc: #{inspect(acc)}") ##For debugging purpose
    flatten(tail, [head | acc])
  end





    @doc """
    This function analyze a string, if it can be read backwards with the same meaning, then the function returns true, if not, it returns false.
  ## Examples

      iex> Tarea2.palindromo("Anita lava la tina")
      OK

  """
  def palindromo(string) do

    cond do

      # Original string in lower case and without spaces
      String.downcase(string)
      |>String.replace(" ", "")       ==
      # Reversed string in lower case and without spaces
      String.downcase(string)
      |>String.replace(" ", "")
      |>String.reverse -> true

      true -> false

    end



  end

end
